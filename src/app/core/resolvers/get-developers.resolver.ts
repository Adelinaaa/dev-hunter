import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Developer } from '../../main/models/developer.model';
import { DevelopersService } from '../../main/services/developers.service';

@Injectable({
  providedIn: 'root'
})
export class GetDevelopersResolver implements Resolve<Developer[]> {
  constructor(private developersService: DevelopersService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Developer[]> | Promise<Developer[]> | Developer[] {
    return this.developersService.getAll$();
  }

}
