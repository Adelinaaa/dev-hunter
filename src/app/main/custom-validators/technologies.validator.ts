import { FormControl } from '@angular/forms';
import { Technology } from '../models/technology.model';

export const uniqueTechnology = (technologies: Technology[], originalName: string) => {
  return (control: FormControl): { [key: string]: any } | null => {
    if (!control.value) {
      return null;
    }

    if (!technologies.find((technology) => technology.name === control.value && technology.name !== originalName)) {
      return null;
    }
    return {technologyNameNotUnique: true};
  };
};
