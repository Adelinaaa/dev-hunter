import { FormControl } from '@angular/forms';
import { Location } from '../models/location.model';

export const uniqueLocation = (locations: Location[], originalName: string) => {
  return (control: FormControl): { [key: string]: any } | null => {
    if (!control.value) {
      return null;
    }

    if (!locations.find((location) => location.name === control.value && location.name !== originalName)) {
      return null;
    }
    return {locationNameNotUnique: true};
  };
};
