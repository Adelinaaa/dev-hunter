import { FormControl } from '@angular/forms';
import { Developer } from '../models/developer.model';

export const uniqueEmail = (developers: Developer[], originalEmail: string) => {
  return (control: FormControl): { [key: string]: any } | null => {
    if (!control.value) {
      return null;
    }

    if (!developers.find((developer) => developer.email === control.value && developer.email !== originalEmail)) {
      return null;
    }
    return {developerEmailNotUnique: true};
  };
};
