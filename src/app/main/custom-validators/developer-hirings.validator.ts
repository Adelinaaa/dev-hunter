import { FormGroup } from '@angular/forms';
import { Developer } from '../models/developer.model';

export const availableDates = (developer: Developer) => {
  return (control: FormGroup): { [key: string]: any } | null => {
    const startDate = control.value.startDate;
    const endDate = control.value.endDate;

    if (!startDate || !endDate) {
      return null;
    }
    if (developer.hirings?.find((hiring) =>
      startDate <= new Date(hiring.range.endDate) && new Date(hiring.range.startDate) <= endDate)) {
      return {theseDatesNotAvailable: true};
    }
    return null;

  };
};
