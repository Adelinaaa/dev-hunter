import { FormControl } from '@angular/forms';
import { User } from '../../auth/models/user.model';

export const uniqueUserEmail = (users: User[]) => {
  return (control: FormControl): { [key: string]: any } | null => {
    if (!control.value) {
      return null;
    }

    if (!users?.find((user) => user.email === control.value)) {
      return null;
    }
    return {userEmailNotUnique: true};
  };
};
