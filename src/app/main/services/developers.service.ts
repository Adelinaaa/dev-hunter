import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Developer } from '../models/developer.model';

@Injectable({
  providedIn: 'root'
})

export class DevelopersService {
  readonly BASE_URL = environment.url;
  readonly DEVELOPERS_URL = `${this.BASE_URL}660/developers`;
  subject$ = new Subject();

  constructor(private http: HttpClient) {
  }

  getAll$(): Observable<Developer[]> {
    return this.http.get<Developer[]>(this.DEVELOPERS_URL, {
      params: new HttpParams()

        .append('_sort', 'id')
        .append('_order', 'desc')
        .set('_expand', 'location')
        .append('_expand', 'technology')
        .append('_embed', 'hirings')
    });
  }

  count$(): Observable<number> {
    return this.http.get<Developer[]>(this.DEVELOPERS_URL, {
      observe: 'response',
      params: new HttpParams()
        .append('_page', '1')
        .append('_limit', '1')
    }).pipe(
      map(response => {
        const totalCount = response.headers.get('X-Total-Count');
        return totalCount ? +totalCount : 0;
      })
    );

  }

  delete$(id: number): Observable<void> {
    return this.http.delete<void>(this.DEVELOPERS_URL + '/' + id);
  }

  save$(developer: Developer): Observable<Developer> {
    if (developer.id) {
      return this.http.put<Developer>(this.DEVELOPERS_URL + '/' + developer.id, developer);
    } else {
      return this.http.post<Developer>(this.DEVELOPERS_URL, developer);
    }
  }

  get$(id: number): Observable<Developer> {
    return this.http.get<Developer>(this.DEVELOPERS_URL + '/' + id, {
      params: new HttpParams()
        .append('_expand', 'location')
        .append('_expand', 'technology')
        .append('_embed', 'hirings')
    });
  }

}
