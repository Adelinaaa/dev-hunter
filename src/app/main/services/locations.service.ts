import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Location } from '../models/location.model';

@Injectable({
  providedIn: 'root'
})

export class LocationsService {
  readonly BASE_URL = environment.url;
  readonly LOCATIONS_URL = `${this.BASE_URL}660/locations`;

  constructor(private http: HttpClient) {

  }

  getAll$(): Observable<Location[]> {
    return this.http.get<Location[]>(this.LOCATIONS_URL, {
      params: new HttpParams()
        .append('_sort', 'id')
        .append('_order', 'desc')
        .append('_embed', 'developers')
    });
  }

  delete$(id: number): Observable<void> {
    return this.http.delete<void>(this.LOCATIONS_URL + '/' + id);
  }

  save$(location: Location): Observable<Location> {
    if (location.id) {
      return this.http.put<Location>(this.LOCATIONS_URL + '/' + location.id, location);
    } else {
      return this.http.post<Location>(this.LOCATIONS_URL, location);
    }
  }
}
