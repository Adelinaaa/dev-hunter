import { Injectable, Renderer2, RendererFactory2 } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeSwitcherService {
  private renderer: Renderer2;
  switchTheme$ = new Subject<void>();

  constructor(rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  loadTheme(): void {
    const localStorageTheme = localStorage.getItem('theme') || 'light-theme';
    this.changeBodyClass(localStorageTheme);
  }

  setTheme(theme: string):void {
    localStorage.setItem('theme', theme);
    this.changeBodyClass(theme);

  }
  private changeBodyClass(theme: string): void {
    if(theme === "dark-theme") {
      this.renderer.addClass(document.body, 'dark-theme')
      this.renderer.removeClass(document.body, 'light-theme')
    } else {
      this.renderer.addClass(document.body, 'light-theme')
      this.renderer.removeClass(document.body, 'dark-theme')
    }
  }

  switchTheme(): void {
    if (this.isThemeDark()) {
      this.setTheme('light-theme');
    } else {
      this.setTheme('dark-theme');
    }
  }

  isThemeDark(): boolean {
    const currentTheme = localStorage.getItem('theme');
    return currentTheme === 'dark-theme';
  }

}
