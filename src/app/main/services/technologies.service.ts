import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AuthService } from '../../auth/services/auth.service';
import { Technology } from '../models/technology.model';

@Injectable({
  providedIn: 'root'
})
export class TechnologiesService {
  readonly BASE_URL = environment.url;
  readonly TECHNOLOGIES_URL = `${this.BASE_URL}660/technologies`

  constructor(private http: HttpClient) {
  }

  getAll$(): Observable<Technology[]> {
    return this.http.get<Technology[]>(this.TECHNOLOGIES_URL, {
      params: new HttpParams()
        .append('_sort', 'id')
        .append('_order', 'desc')
        .append('_embed', 'developers')

    });
  }
  delete$(id: number): Observable<void> {
    return this.http.delete<void>(this.TECHNOLOGIES_URL + '/' + id);
  }

  save$(technology: Technology): Observable<Technology> {
    if (technology.id) {
      return this.http.put<Technology>(this.TECHNOLOGIES_URL + '/' + technology.id, technology);
    } else {
      return this.http.post<Technology>(this.TECHNOLOGIES_URL, technology);
    }
  }

}
