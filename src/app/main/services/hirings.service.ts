import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Hire } from '../models/hire.model';

@Injectable({
  providedIn: 'root'
})

export class HiringsService {
  readonly BASE_URL = environment.url;
  readonly HIRINGS_URL = `${this.BASE_URL}660/hirings`;

  constructor(private http: HttpClient) {

  }

  getAll$(): Observable<Hire[]> {
    return this.http.get<Hire[]>(this.HIRINGS_URL, {
      params: new HttpParams()
        .append('_sort', 'id')
        .append('_order', 'desc')
        .append('_expand', 'developer')
    });
  }

  delete$(id: number): Observable<void> {
    return this.http.delete<void>(this.HIRINGS_URL + '/' + id);
  }

  save$(hiring: Hire): Observable<Hire> {
    if (hiring.id) {
      return this.http.put<Hire>(this.HIRINGS_URL + '/' + hiring.id, hiring);
    } else {
      return this.http.post<Hire>(this.HIRINGS_URL, hiring);
    }
  }
}
