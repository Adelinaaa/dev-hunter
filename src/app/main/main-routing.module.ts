import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GetDevelopersResolver } from '../core/resolvers/get-developers.resolver';
import { LoggedUserResolver } from '../core/resolvers/logged-user.resolver';
import { DeveloperDetailComponent } from './components/developers/developer-detail/developer-detail.component';
import { DevelopersEditDialogComponent } from './components/developers/developers-edit-dialog/developers-edit-dialog.component';
import { DevelopersComponent } from './components/developers/developers.component';
import { HireTeamComponent } from './components/hire-team/hire-team.component';
import { HiringsComponent } from './components/hirings/hirings.component';
import { LocationsComponent } from './components/locations/locations.component';
import { TechnologiesComponent } from './components/technologies/technologies.component';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    resolve: {loggedUser: LoggedUserResolver},
    children: [
      {
        path: '',
        redirectTo: 'developers',
        pathMatch: 'full',
      },
      {
        path: 'locations',
        component: LocationsComponent
      },
      {
        path: 'developers',
        component: DevelopersComponent
      },
      {
        path: 'technologies',
        component: TechnologiesComponent
      },
      {
        path: 'developers/create',
        component: DevelopersEditDialogComponent,
        resolve: {allDevelopers: GetDevelopersResolver}
      },
      {
        path: 'developers/:id',
        component: DeveloperDetailComponent,
        resolve: {allDevelopers: GetDevelopersResolver}
      },
      {
        path: 'developers/edit/:id',
        component: DevelopersEditDialogComponent,
        resolve: {allDevelopers: GetDevelopersResolver}
      },
      {
        path: 'hirings',
        component: HiringsComponent,
      },
      {
        path: 'hire-a-team',
        component: HireTeamComponent,
        resolve: {allDevelopers: GetDevelopersResolver}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
