export enum LanguageEnum {
  ENGLISH = 'English',
  BULGARIAN = 'Bulgarian',
  SERBIAN = 'Serbian',
}
