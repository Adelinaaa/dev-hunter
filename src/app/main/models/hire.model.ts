export class Hire {
  id: number;
  range: {
    startDate: Date;
    endDate: Date;
  };
  developerId: number;
}
