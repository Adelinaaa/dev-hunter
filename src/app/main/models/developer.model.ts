import { LanguageEnum } from '../enums/language.enum';
import { Hire } from './hire.model';
import { Technology } from './technology.model';
import { Location } from './location.model'

export class Developer {
  id: number;
  name: string;
  email: string;
  phoneNum: string;

  location: Location;
  locationId: number;
  technology: Technology;
  technologyId: number;

  pricePerHour: number;
  yearsOfExp: number;
  nativeLang: LanguageEnum;
  profilePic: string;
  description: string;
  linkedIn: string;

  hirings: Hire[];
}
