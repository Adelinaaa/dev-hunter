import { Developer } from './developer.model';

export class Location {
  id: number;
  name: string;
  mapLink: string;
  developers: Developer[];
}
