import { Developer } from './developer.model';

export class Technology {
  id: number;
  name: string;
  imageUrl: string;
  developers: Developer[];
}
