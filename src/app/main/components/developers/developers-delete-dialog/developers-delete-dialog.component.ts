import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { Developer } from '../../../models/developer.model';
import { DevelopersService } from '../../../services/developers.service';

@Component({
  selector: 'app-developers-delete-dialog',
  templateUrl: './developers-delete-dialog.component.html',
  styleUrls: ['./developers-delete-dialog.component.scss']
})
export class DevelopersDeleteDialogComponent implements OnInit {
  developer: Developer;

  constructor(private developersService: DevelopersService, @Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialogRef<DevelopersDeleteDialogComponent>) {
    this.developer = data.developer;
  }

  ngOnInit(): void {
  }

  onConfirmDelete(developer) {
    this.developersService.delete$(developer.id).pipe(take(1)).subscribe(() => {
      this.developersService.subject$.next();
    });
    this.dialog.close(developer);
  }

  onCancel(): void {
    this.dialog.close();
  }
}
