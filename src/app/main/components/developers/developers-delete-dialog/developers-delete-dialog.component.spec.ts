import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopersDeleteDialogComponent } from './developers-delete-dialog.component';

describe('DevelopersDeleteDialogComponent', () => {
  let component: DevelopersDeleteDialogComponent;
  let fixture: ComponentFixture<DevelopersDeleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DevelopersDeleteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopersDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
