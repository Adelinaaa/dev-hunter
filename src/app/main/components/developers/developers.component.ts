import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Developer } from '../../models/developer.model';
import { Location } from '../../models/location.model';
import { Technology } from '../../models/technology.model';
import { DevelopersService } from '../../services/developers.service';
import { LocationsService } from '../../services/locations.service';
import { TechnologiesService } from '../../services/technologies.service';
import { DevelopersEditDialogComponent } from './developers-edit-dialog/developers-edit-dialog.component';

@Component({
  selector: 'app-developers',
  templateUrl: './developers.component.html',
  styleUrls: ['./developers.component.scss']
})
export class DevelopersComponent implements OnInit {
  developers: Developer[];
  technologies: Technology[];
  locations: Location[];
  developer: Developer;
  isLoading = false;

  constructor(private developersService: DevelopersService,
              private technologiesService: TechnologiesService,
              private locationsService: LocationsService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getDevelopers();
  }
  openDetails(developer: Developer):void {
    this.router.navigate(['developers', developer.id]).then();
  }

  private getDevelopers(): void {
    this.developersService.getAll$().pipe(take(1)).subscribe(
      (developers) => {
        this.developers = developers;
      }
    );
  }

  addDeveloper(): void {
    this.router.navigate(['developers', 'create']);
  }
}
