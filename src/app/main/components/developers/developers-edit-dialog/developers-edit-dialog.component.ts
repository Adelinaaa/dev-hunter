import {Overlay} from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { uniqueEmail } from '../../../custom-validators/email.validator';
import { LanguageEnum } from '../../../enums/language.enum';
import { Developer } from '../../../models/developer.model';
import { Location } from '../../../models/location.model';
import { Technology } from '../../../models/technology.model';
import { DevelopersService } from '../../../services/developers.service';
import { LocationsService } from '../../../services/locations.service';
import { TechnologiesService } from '../../../services/technologies.service';
import { LocationsEditDialogComponent } from '../../locations/locations-edit-dialog/locations-edit-dialog.component';
import { TechnologiesEditDialogComponent } from '../../technologies/technologies-edit-dialog/technologies-edit-dialog.component';
import { HireDeveloperComponent } from '../hire-developer/hire-developer.component';

@Component({
  selector: 'app-developers-edit-dialog',
  templateUrl: './developers-edit-dialog.component.html',
  styleUrls: ['./developers-edit-dialog.component.scss']
})
export class DevelopersEditDialogComponent implements OnInit {
  formGroup: FormGroup;
  developer: Developer;
  locations: Location[];
  technologies: Technology[];
  developers: Developer[];

  public languageTypes = Object.values(LanguageEnum);

  constructor(private fb: FormBuilder,
              private developersService: DevelopersService,
              private technologiesService: TechnologiesService,
              private locationsService: LocationsService,
              private matSnackBar: MatSnackBar,
              private addLocationDialog: MatDialog,
              private addTechnologyDialog: MatDialog,
              private route: ActivatedRoute,
              private router: Router,
              private overlay: Overlay) {
    this.developers = this.route.snapshot.data.allDevelopers;
  }

  ngOnInit(): void {
    this.getLocations();
    this.getTechnologies();
    const devId = Number(this.route.snapshot.paramMap.get('id'));

    if (devId) {
      this.developersService.get$(devId).subscribe((developer) => {
        this.developer = developer;
        this.buildForm();
      });
    } else {
      this.developer = new Developer();
      this.buildForm();
    }
  }

  private getTechnologies(): void {
    this.technologiesService.getAll$().pipe(take(1)).subscribe(
      (technologies) => {
        this.technologies = technologies;
      });
  }

  private getLocations(): void {
    this.locationsService.getAll$().pipe(take(1)).subscribe(
      (locations) => {
        this.locations = locations;
      });
  }


  onSubmit(): void {

    if (this.formGroup.invalid) {
      return;
    }

    this.developersService
      .save$(this.formGroup.value).pipe(take(1))
      .subscribe((result) => {
        this.developersService.subject$.next();
        this.matSnackBar.open('Developer is saved!', 'OK', {
          duration: 2000
        });
        this.router.navigate(['developers']);
      });
  }

  closeEditForm(): void {
    this.router.navigate(['developers', this.developer.id]);
  }

  closeCreateForm(): void {
    this.router.navigate(['developers']);
  }

  createLocation(): void {
    let dialogRef = this.addLocationDialog.open(LocationsEditDialogComponent, {
      data: {location: new Location(), locations: this.locations},
      width: '300px',
      scrollStrategy: this.overlay.scrollStrategies.noop()
    });

    dialogRef
      .afterClosed().pipe(take(1))
      .subscribe((result) => {
        if (result) {
          this.matSnackBar.open('Location is created!', 'Cancel', {
            duration: 2000
          });
        }
        this.getLocations()
      });
  }

  createTechnology(): void {
    let dialogRef = this.addTechnologyDialog.open(TechnologiesEditDialogComponent, {
      data: {technology: new Technology(), technologies: this.technologies},
      width: '300px',
      scrollStrategy: this.overlay.scrollStrategies.noop()
    });
    dialogRef
      .afterClosed().pipe(take(1))
      .subscribe((result) => {
        if (result) {
          this.matSnackBar.open('Technology is created!', 'Cancel', {
            duration: 2000
          });
        }
        this.getTechnologies()
      });
  }



  private buildForm(): void {
    this.formGroup = this.fb.group({
      id: [this.developer.id],
      name: [
        this.developer.name,
        Validators.required
      ],
      email: [this.developer.email, [Validators.required, Validators.email, uniqueEmail(this.developers, this.developer.email)
      ]],
      phoneNum: [this.developer.phoneNum, [Validators.pattern(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/), Validators.required]],
      locationId: [this.developer.locationId, Validators.required],
      technologyId: [this.developer.technologyId, Validators.required],
      pricePerHour: [this.developer.pricePerHour, [Validators.required, Validators.min(1), Validators.pattern(/^\d{0,8}(\.\d{1,4})?$/)]],
      yearsOfExp: [this.developer.yearsOfExp, [Validators.required, Validators.min(0), Validators.max(50), Validators.pattern(/^-?(0|[1-9]\d*)?$/)]],
      nativeLang: [this.developer.nativeLang, Validators.required],
      profilePic: [this.developer.profilePic, Validators.pattern(
        /(https:\/\/.*\.(?:png|jpg))/i
      )],
      description: [this.developer.description],
      linkedIn: [this.developer.linkedIn, Validators.pattern(/(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\/(pub|in|profile)/)]
    });
  }

}


