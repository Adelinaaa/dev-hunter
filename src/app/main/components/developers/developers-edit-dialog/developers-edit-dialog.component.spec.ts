import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DevelopersEditDialogComponent } from './developers-edit-dialog.component';

describe('DevelopersEditDialogComponent', () => {
  let component: DevelopersEditDialogComponent;
  let fixture: ComponentFixture<DevelopersEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DevelopersEditDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DevelopersEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
