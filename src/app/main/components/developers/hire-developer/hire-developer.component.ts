import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs/operators';
import { availableDates } from '../../../custom-validators/developer-hirings.validator';
import { Developer } from '../../../models/developer.model';
import { Hire } from '../../../models/hire.model';
import { HiringsService } from '../../../services/hirings.service';

@Component({
  selector: 'app-hire-developer',
  templateUrl: './hire-developer.component.html',
  styleUrls: ['./hire-developer.component.scss']
})
export class HireDeveloperComponent implements OnInit {
  formGroup: FormGroup;
  hiring: Hire;
  developer: Developer;
  today = new Date();



  constructor(private fb: FormBuilder,
              private dialog: MatDialogRef<HireDeveloperComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private hiringsService: HiringsService,
              private matSnackBar: MatSnackBar) {

    this.hiring = data.hiring;
    this.developer = data.developer;
  }

  /*Disable unavailable dates in calendar*/
  rangeFilter = (date: Date | null):  boolean => {
    if(!date) {
      return false
    }
    for(let i = 0; i< this.developer.hirings.length; i++){
      const {startDate, endDate} = this.developer.hirings[i].range;
      if(date >= new Date(startDate) && date <= new Date(endDate)){
        return false
      }
    }
    return true;
}

  ngOnInit(): void {
    this.buildForm();

  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }
    this.hiringsService
      .save$(this.formGroup.value).pipe(take(1))
      .subscribe((result) => {
        this.matSnackBar.open(this.developer.name + ' is hired for this period!', 'OK', {
          duration: 2000
        });
        this.dialog.close(result);
      });
  }

  private buildForm(): void {

    this.formGroup = this.fb.group({
      id: [null],
      range: this.fb.group({
        startDate: [
          null, Validators.required
        ],
        endDate: [null, Validators.required]
      }, {
        validator: [availableDates(this.developer)]
      }),
      developerId: [this.developer.id]
    });
  }

}
