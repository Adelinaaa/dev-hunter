import { Overlay } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Developer } from '../../../models/developer.model';
import { Hire } from '../../../models/hire.model';
import { DevelopersService } from '../../../services/developers.service';
import { DevelopersDeleteDialogComponent } from '../developers-delete-dialog/developers-delete-dialog.component';
import { HireDeveloperComponent } from '../hire-developer/hire-developer.component';

@Component({
  selector: 'app-developer-detail',
  templateUrl: './developer-detail.component.html',
  styleUrls: ['./developer-detail.component.scss']
})
export class DeveloperDetailComponent implements OnInit {
  developer: Developer;

  constructor(private deleteDialog: MatDialog,
              private editDialog: MatDialog,
              private matSnackBar: MatSnackBar,
              private route: ActivatedRoute,
              private developersService: DevelopersService,
              private router: Router,
              private overlay: Overlay,
              private hireDevDialog: MatDialog) {
  }

  ngOnInit(): void {
    const devId = Number(this.route.snapshot.paramMap.get('id'));
    this.developersService.get$(devId).subscribe((developer) => {
      this.developer = developer;
    });
  }


  deleteDeveloper(developer: Developer, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.deleteDialog.open(DevelopersDeleteDialogComponent, {
      data: {developer: developer},
      width: '300px'
    });
    dialogRef
      .afterClosed().pipe(take(1))
      .subscribe((result) => {
        if (result) {
          this.matSnackBar.open('Developer is deleted!', 'Cancel', {
            duration: 2000
          });
          this.router.navigate(['developers']);
        }
      });
  }

  editDeveloper(developer: Developer): void {
    this.router.navigate(['developers', 'edit', developer.id]);
  }

  hireDeveloper(developer: Developer): void {
    let dialogRef = this.hireDevDialog.open(HireDeveloperComponent,

      {
        data: {hiring: new Hire(), developer: developer},
        width: '340px',
        scrollStrategy: this.overlay.scrollStrategies.noop()
      });
    dialogRef
      .afterClosed().pipe(take(1))
      .subscribe((result) => {
        if (result) {
          this.developer.hirings.push(result);
        }
      });
  }
}
