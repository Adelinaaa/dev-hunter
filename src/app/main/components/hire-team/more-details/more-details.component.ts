import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Developer } from '../../../models/developer.model';

@Component({
  selector: 'app-more-details',
  templateUrl: './more-details.component.html',
  styleUrls: ['./more-details.component.scss']
})
export class MoreDetailsComponent implements OnInit {
  developer: Developer;
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialogRef<MoreDetailsComponent>) {
    this.developer = data.developer
  }

  ngOnInit(): void {
  }

  close(): void {
    this.dialog.close()
  }

}
