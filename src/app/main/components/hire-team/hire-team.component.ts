import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Developer } from '../../models/developer.model';
import { Hire } from '../../models/hire.model';
import { HiringsService } from '../../services/hirings.service';
import { MoreDetailsComponent } from './more-details/more-details.component';

@Component({
  selector: 'app-hire-team',
  templateUrl: './hire-team.component.html',
  styleUrls: ['./hire-team.component.scss']
})
export class HireTeamComponent implements OnInit {
  formGroup: FormGroup;
  developers: Developer[];
  filteredDevs;
  today = new Date();

  constructor(private hireTeamSuccess: MatDialog, private datePipe: DatePipe, private matSnackBar: MatSnackBar, private router: Router, private route: ActivatedRoute, private moreInfoDialog: MatDialog, private fb: FormBuilder, private hiringsService: HiringsService) {
    this.developers = this.route.snapshot.data.allDevelopers;

  }

  ngOnInit(): void {
    this.buildForm();
  }


  moreDetails(developer: Developer, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.moreInfoDialog.open(MoreDetailsComponent, {
      data: {developer: developer},
      width: '400px'
    });
  }


  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }

    const startDate = this.formGroup.get('range.startDate')?.value;
    const endDate = this.formGroup.get('range.endDate')?.value;
    const developerIds: number[] = this.formGroup.get('developers')?.value;


    const observables: Observable<Hire>[] = [];

    developerIds.forEach(developerId => {
      const hire = new Hire();
      hire.range = {startDate, endDate};
      hire.developerId = developerId;

      observables.push(this.hiringsService.save$(hire));

    });

    forkJoin(observables).pipe(take(1)).subscribe({
      next: () => {
        this.matSnackBar.open('Team is hired for period: ' + this.datePipe.transform(startDate, 'yyyy-MM-dd') + ' : ' + this.datePipe.transform(endDate, 'yyyy-MM-dd'), 'OK', {
          duration: 5000

        });
        /*Clear form and validation when form is submitted*/
        this.formGroup.reset();
        this.formGroup.get('developers')?.clearValidators();
        this.formGroup.get('range.startDate')?.clearValidators();
        this.formGroup.get('range.endDate')?.clearValidators();
        this.formGroup.get('developers')?.updateValueAndValidity();
        this.formGroup.get('range.startDate')?.updateValueAndValidity();
        this.formGroup.get('range.endDate')?.updateValueAndValidity();
      }
    });

  }

  private buildForm(): void {

    this.formGroup = this.fb.group({
      range: this.fb.group({
          startDate: [
            null, Validators.required
          ],
          endDate: [null, Validators.required]
        }
      ),
      developers: [[], Validators.required]

    });

    this.formGroup.get('range')?.valueChanges.subscribe(value => {
      this.filteredDevs = this.developers.filter(developer => !developer.hirings?.find(hiring => new Date(hiring.range.startDate) <= new Date(value.endDate) &&
        (new Date(hiring.range.endDate) >= new Date(value.startDate))));
      this.formGroup.get('developers')?.setValue([]);
    });
  }
}
