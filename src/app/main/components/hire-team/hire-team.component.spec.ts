import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HireTeamComponent } from './hire-team.component';

describe('HireTeamComponent', () => {
  let component: HireTeamComponent;
  let fixture: ComponentFixture<HireTeamComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HireTeamComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HireTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
