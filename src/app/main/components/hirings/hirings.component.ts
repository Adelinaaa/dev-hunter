import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { take } from 'rxjs/operators';
import { Hire } from '../../models/hire.model';
import { HiringsService } from '../../services/hirings.service';

@Component({
  selector: 'app-hirings',
  templateUrl: './hirings.component.html',
  styleUrls: ['./hirings.component.scss']
})
export class HiringsComponent implements OnInit {
  displayedColumns: string[] = ['developer', 'startDate', 'endDate'];
  dataSource;
  hirings: Hire[];

  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private hiringsService: HiringsService) { }

  ngOnInit(): void {
    this.getHirings()

  }

  getHirings(): void {
    this.hiringsService.getAll$().pipe(take(1)).subscribe(
      (hirings) => {
        this.dataSource = new MatTableDataSource(hirings);
        this.hirings = hirings;
        this.dataSource.paginator = this.paginator;
        this.totalSize = this.hirings.length;
      }
    );
  }

  public handlePagination(e: any): void {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.hirings.slice(start, end);
    this.dataSource = part;
  }
}
