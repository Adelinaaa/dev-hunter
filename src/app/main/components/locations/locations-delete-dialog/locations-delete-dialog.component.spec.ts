import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationsDeleteDialogComponent } from './locations-delete-dialog.component';

describe('LocationsDeleteDialogComponent', () => {
  let component: LocationsDeleteDialogComponent;
  let fixture: ComponentFixture<LocationsDeleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationsDeleteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationsDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
