import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { Location } from '../../../models/location.model';
import { LocationsService } from '../../../services/locations.service';


@Component({
  selector: 'app-locations-delete-dialog',
  templateUrl: './locations-delete-dialog.component.html',
  styleUrls: ['./locations-delete-dialog.component.scss']
})
export class LocationsDeleteDialogComponent implements OnInit {
  location: Location;

  constructor(
    private locationsService: LocationsService, @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialogRef<LocationsDeleteDialogComponent>) {
    this.location = data.location;
  }

  ngOnInit(): void {
  }

  onConfirmDelete(location) {
    this.locationsService.delete$(location.id).pipe(take(1)).subscribe();
    this.dialog.close(location);
  }

  onCancel(): void {
    this.dialog.close();
  }

}
