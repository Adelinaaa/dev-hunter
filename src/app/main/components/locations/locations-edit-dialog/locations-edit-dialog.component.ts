import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs/operators';
import { uniqueLocation } from '../../../custom-validators/locations.validator';
import { Location } from '../../../models/location.model';
import { LocationsService } from '../../../services/locations.service';


@Component({
  selector: 'app-locations-edit-dialog',
  templateUrl: './locations-edit-dialog.component.html',
  styleUrls: ['./locations-edit-dialog.component.scss']
})
export class LocationsEditDialogComponent implements OnInit {
  formGroup: FormGroup;
  location: Location;
  locations: Location[];

  constructor(private fb: FormBuilder,
              private locationsService: LocationsService,
              private matSnackBar: MatSnackBar,
              private dialog: MatDialogRef<LocationsEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.location = data.location;
    this.locations = data.locations;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }

    this.locationsService
      .save$(this.formGroup.value).pipe(take(1))
      .subscribe((result) => {
        this.matSnackBar.open('Location is saved!', 'OK', {
          duration: 2000
        });
        this.dialog.close(result);
      });
  }

  close(): void {
    this.dialog.close();
  }

  private buildForm(): void {
    this.formGroup = this.fb.group({
      id: [this.location.id],
      name: [
        this.location.name,
        [Validators.required, uniqueLocation(this.locations, this.location.name)]
      ],
      mapLink: [this.location.mapLink, [Validators.pattern(
        /^https?\:\/\/(www\.)?goo.gl\/maps\b/
      )]]
    });
  }
}
