import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationsEditDialogComponent } from './locations-edit-dialog.component';

describe('LocationsEditDialogComponent', () => {
  let component: LocationsEditDialogComponent;
  let fixture: ComponentFixture<LocationsEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LocationsEditDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationsEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
