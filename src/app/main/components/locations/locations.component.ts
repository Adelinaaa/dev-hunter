import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { take } from 'rxjs/operators';
import { Location } from '../../models/location.model';
import { LocationsService } from '../../services/locations.service';
import { LocationsDeleteDialogComponent } from './locations-delete-dialog/locations-delete-dialog.component';
import { LocationsEditDialogComponent } from './locations-edit-dialog/locations-edit-dialog.component';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss']
})
export class LocationsComponent implements OnInit {
  locations: Location[];
  location: Location;
  displayedColumns: string[] = ['position', 'name', 'mapLink', 'actions'];
  dataSource;
  isLoading = true;

  /* Pagination variables */

  public pageSize = 5;
  public currentPage = 0;
  public totalSize = 0;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private locationsService: LocationsService,
              private dialog: MatDialog,
              private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.getLocations();
  }

  public handlePagination(e: any): void {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }


  getLocations(): void {
    this.locationsService.getAll$().pipe(take(1)).subscribe(
      (locations) => {
        this.dataSource = new MatTableDataSource(locations);
        this.locations = locations;
        this.dataSource.paginator = this.paginator;
        this.totalSize = this.locations.length;
      }
    );
    this.isLoading = false;
  }

  editLocation(location: Location, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.dialog.open(LocationsEditDialogComponent, {
      data: {location: location, locations: this.locations},
      width: '500px'
    });

    dialogRef
      .afterClosed().pipe(take(1))
      .subscribe((result) => {
        this.isLoading = true;
        if (result) {
          this.getLocations();
        }
        this.isLoading = false;
      });

  }

  addLocation(): void {
    this.editLocation(new Location());
  }

  deleteLocation(location: Location, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.dialog.open(LocationsDeleteDialogComponent, {
      data: {location: location},
      width: '300px'
    });
    dialogRef
      .afterClosed().pipe(take(1))
      .subscribe((result) => {
        this.isLoading = true;
        if (result) {
          this.matSnackBar.open('Location is deleted!', 'Cancel', {
            duration: 2000
          });
        }
        this.getLocations();
        this.isLoading = false;
      });
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.locations.slice(start, end);
    this.dataSource = part;
  }

}
