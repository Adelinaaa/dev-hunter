import { Component, OnInit } from '@angular/core';
import { ThemeSwitcherService } from '../../services/theme-switcher.service';

@Component({
  selector: 'app-theme-switcher',
  templateUrl: './theme-switcher.component.html',
  styleUrls: ['./theme-switcher.component.scss']
})
export class ThemeSwitcherComponent implements OnInit {
  isDark: boolean;

  constructor(private themeSwitcherService: ThemeSwitcherService) {
    this.isDark = this.themeSwitcherService.isThemeDark();
  }

  switchTheme(): void {
    this.themeSwitcherService.switchTheme$.next();
  }

  ngOnInit(): void {
  }

}
