import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs/operators';
import { uniqueTechnology } from '../../../custom-validators/technologies.validator';
import { Technology } from '../../../models/technology.model';
import { TechnologiesService } from '../../../services/technologies.service';

@Component({
  selector: 'app-technologies-edit-dialog',
  templateUrl: './technologies-edit-dialog.component.html',
  styleUrls: ['./technologies-edit-dialog.component.scss']
})
export class TechnologiesEditDialogComponent implements OnInit {
  formGroup: FormGroup;
  technology: Technology;
  technologies: Technology[];

  constructor(private fb: FormBuilder,
              private technologiesService: TechnologiesService,
              private matSnackBar: MatSnackBar,
              private dialog: MatDialogRef<TechnologiesEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.technology = data.technology;
    this.technologies = data.technologies;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }

    this.technologiesService
      .save$(this.formGroup.value).pipe(take(1))
      .subscribe((result) => {
        this.matSnackBar.open('Technology is saved!', 'OK', {
          duration: 2000
        });
        this.dialog.close(result);
      });
  }

  close(): void {
    this.dialog.close();
  }

  private buildForm(): void {
    this.formGroup = this.fb.group({
      id: [this.technology.id],
      name: [
        this.technology.name,
        [Validators.required, uniqueTechnology(this.technologies, this.technology.name)]
      ],
      imageUrl: [this.technology.imageUrl, [Validators.pattern(
        /(https:\/\/.*\.(?:png|jpg))/i
      )]]
    });
  }
}
