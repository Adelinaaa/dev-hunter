import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologiesEditDialogComponent } from './technologies-edit-dialog.component';

describe('TechnologiesEditDialogComponent', () => {
  let component: TechnologiesEditDialogComponent;
  let fixture: ComponentFixture<TechnologiesEditDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnologiesEditDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnologiesEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
