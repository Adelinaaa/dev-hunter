import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs/operators';
import { Location } from '../../models/location.model';
import { Technology } from '../../models/technology.model';
import { TechnologiesService } from '../../services/technologies.service';
import { LocationsDeleteDialogComponent } from '../locations/locations-delete-dialog/locations-delete-dialog.component';
import { LocationsEditDialogComponent } from '../locations/locations-edit-dialog/locations-edit-dialog.component';
import { TechnologiesDeleteDialogComponent } from './technologies-delete-dialog/technologies-delete-dialog.component';
import { TechnologiesEditDialogComponent } from './technologies-edit-dialog/technologies-edit-dialog.component';

@Component({
  selector: 'app-technologies',
  templateUrl: './technologies.component.html',
  styleUrls: ['./technologies.component.scss']
})
export class TechnologiesComponent implements OnInit {
  technologies: Technology[];
  isLoading = true;

  constructor(private technologiesService: TechnologiesService, private dialog: MatDialog, private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.getTechnologies();
  }

  getTechnologies(): void {
    this.technologiesService.getAll$().pipe(take(1)).subscribe(
      (technologies) => {
        this.technologies = technologies;
      }
    );
    this.isLoading = false;
  }

  deleteTechnology(technology: Technology, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.dialog.open(TechnologiesDeleteDialogComponent, {
      data: {technology: technology},
      width: '300px'
    });
    dialogRef
      .afterClosed().pipe(take(1))
      .subscribe((result) => {
        this.isLoading = true;
        if (result) {
          this.matSnackBar.open('Technology is deleted!', 'Cancel', {
            duration: 2000
          });
        }
        this.getTechnologies();
        this.isLoading = false;
      });
  }

  editTechnology(technology: Technology, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }
    let dialogRef = this.dialog.open(TechnologiesEditDialogComponent, {
      data: {technology: technology, technologies: this.technologies},
      width: '500px'
    });

    dialogRef
      .afterClosed().pipe(take(1))
      .subscribe((result) => {
        this.isLoading = true;
        if (result) {
          this.getTechnologies();
        }
        this.isLoading = false;
      });

  }

  addTechnology(): void {
    this.editTechnology(new Technology());
  }

}
