import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologiesDeleteDialogComponent } from './technologies-delete-dialog.component';

describe('TechnologiesDeleteDialogComponent', () => {
  let component: TechnologiesDeleteDialogComponent;
  let fixture: ComponentFixture<TechnologiesDeleteDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnologiesDeleteDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TechnologiesDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
