import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { take } from 'rxjs/operators';
import { Technology } from '../../../models/technology.model';
import { TechnologiesService } from '../../../services/technologies.service';

@Component({
  selector: 'app-technologies-delete-dialog',
  templateUrl: './technologies-delete-dialog.component.html',
  styleUrls: ['./technologies-delete-dialog.component.scss']
})
export class TechnologiesDeleteDialogComponent implements OnInit {
  technology: Technology;

  constructor(private technologiesService: TechnologiesService, @Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialogRef<TechnologiesDeleteDialogComponent>) {
    this.technology = data.technology;
  }

  ngOnInit(): void {
  }

  onConfirmDelete(technology) {
    this.technologiesService.delete$(technology.id).pipe(take(1)).subscribe();
    this.dialog.close(technology);
  }

  onCancel(): void {
    this.dialog.close();
  }

}
