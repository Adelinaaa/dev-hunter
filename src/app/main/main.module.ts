import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../shared/material-ui/material-ui.module';
import { SharedModule } from '../shared/shared.module';

import { DevelopersComponent } from './components/developers/developers.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LocationsComponent } from './components/locations/locations.component';
import { TechnologiesComponent } from './components/technologies/technologies.component';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { LocationsEditDialogComponent } from './components/locations/locations-edit-dialog/locations-edit-dialog.component';
import { LocationsDeleteDialogComponent } from './components/locations/locations-delete-dialog/locations-delete-dialog.component';
import { TechnologiesDeleteDialogComponent } from './components/technologies/technologies-delete-dialog/technologies-delete-dialog.component';
import { TechnologiesEditDialogComponent } from './components/technologies/technologies-edit-dialog/technologies-edit-dialog.component';
import { DeveloperDetailComponent } from './components/developers/developer-detail/developer-detail.component';
import { DevelopersDeleteDialogComponent } from './components/developers/developers-delete-dialog/developers-delete-dialog.component';
import { DevelopersEditDialogComponent } from './components/developers/developers-edit-dialog/developers-edit-dialog.component';
import { ThemeSwitcherComponent } from './components/theme-switcher/theme-switcher.component';
import { HireDeveloperComponent } from './components/developers/hire-developer/hire-developer.component';
import { HiringsComponent } from './components/hirings/hirings.component';
import { HireTeamComponent } from './components/hire-team/hire-team.component';
import { MoreDetailsComponent } from './components/hire-team/more-details/more-details.component';

@NgModule({
  imports: [
    ReactiveFormsModule,
    HttpClientModule,
    MainRoutingModule,
    CommonModule,
    SharedModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [
    MainComponent,
    LocationsComponent,
    DevelopersComponent,
    TechnologiesComponent,
    LocationsEditDialogComponent,
    LocationsDeleteDialogComponent,
    TechnologiesDeleteDialogComponent,
    TechnologiesEditDialogComponent,
    DeveloperDetailComponent,
    DevelopersDeleteDialogComponent,
    DevelopersEditDialogComponent,
    ThemeSwitcherComponent,
    HireDeveloperComponent,
    HiringsComponent,
    HireTeamComponent,
    MoreDetailsComponent
  ]
})
export class MainModule {
}
