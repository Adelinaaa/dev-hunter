import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { User } from '../auth/models/user.model';
import { AuthService } from '../auth/services/auth.service';
import { DevelopersService } from './services/developers.service';
import { ThemeSwitcherService } from './services/theme-switcher.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  developersCount = 0;
  loggedUser: User;
  isOpen: boolean;
  destroy$ = new Subject<boolean>();


  constructor(private developersService: DevelopersService,
              private authService: AuthService,
              private themeSwitcherService: ThemeSwitcherService,
              private router: Router) {
    this.loggedUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.getDevsCount();
    this.themeSwitcherService.loadTheme();

    this.themeSwitcherService.switchTheme$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.themeSwitcherService.switchTheme();
      });
    this.developersService.subject$.pipe(takeUntil(this.destroy$)).subscribe(() => {
        this.getDevsCount()
      }
    );
  }

  getDevsCount(): void {
    this.developersService.count$().pipe(take(1)).subscribe(
      (count) => {
        this.developersCount = count;
      }
    );
  }

  openMenu(): void {
    this.isOpen = true;
  }

  closeMenu(): void {
    this.isOpen = false;
  }

  logOut(): void {
    this.authService.logOut();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  openHirings(): void {
    this.router.navigate(['hirings']);
  }

}
