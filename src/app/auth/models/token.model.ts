export class Token {
  email: string;
  iat: Date;
  exp: Date;
  sub: number;
}
