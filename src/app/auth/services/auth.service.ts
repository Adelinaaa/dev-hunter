import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Token } from '../models/token.model';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  jwtHelperService: JwtHelperService;
  readonly BASE_URL = environment.url;
  readonly REGISTER_URL = `${this.BASE_URL}register`;
  readonly LOGIN_URL = `${this.BASE_URL}login`;
  loggedUser: User;

  constructor(private http: HttpClient) {
    this.jwtHelperService = new JwtHelperService();
  }

  storeToken(token) {
    localStorage.setItem('access_token', token);
  }

  decodeToken(): Token | null {
    const token = localStorage.getItem('access_token');
    if (!token) {
      return null;
    }
    return this.jwtHelperService.decodeToken(token);
  }

  isAccessTokenExpired(): boolean {
    const token = localStorage.getItem('access_token');
    if (!token) {
      return true;
    }
    try {
      return this.jwtHelperService.isTokenExpired(token);

    } catch {
      return true;
    }
  }

  login(user: User): Observable<string> {
    return this.http.post<string>(this.LOGIN_URL, user, {
      responseType: 'text' as 'json'
    });
  }

  logOut(): void {
    localStorage.removeItem('access_token');
    location.replace(`${location.origin}/auth`);
  }

  register(user: User): Observable<string> {
    return this.http.post<string>(this.REGISTER_URL, user, {
      responseType: 'text' as 'json'
    });
  }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(this.BASE_URL + 'users');
  }

  getLoggedUser(): Observable<User> {
    const token = this.decodeToken();
    return this.http.get<User>(
      this.BASE_URL + `users/${token?.sub}`
    ).pipe(
      tap(response => {this.loggedUser = response})
    );
  }
}
