import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { take, takeUntil } from 'rxjs/operators';
import { uniqueEmail } from '../../../main/custom-validators/email.validator';
import { uniqueUserEmail } from '../../../main/custom-validators/user-email.validator';
import { User } from '../../models/user.model';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  hidePassword: boolean = true;
  formGroup: FormGroup;
  destroy$ = new Subject<boolean>();
  users: User[];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.getUsers();
    this.buildForm();


  }

  private getUsers(): void {
    this.authService.getAll().pipe(take(1)).subscribe(
      (users) => {
        this.users = users;
        console.log(this.users)
      }
    );
  }

  private buildForm(): void {
    this.formGroup = this.formBuilder.group({
      fName: [null, Validators.required],
      lName: [null, Validators.required],
      email: [null, [Validators.required, Validators.email]],
      password: [
        null,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[ !"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~])[0-9a-zA-Z !"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~]+/
          )
        ]
      ]
    });
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }

    this.authService
      .register(this.formGroup.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (token) => {
          this.authService.storeToken(Object.values(JSON.parse(token))[0]);
          this.router.navigate(['']);
        },
        (response) => {
          console.log(response);
          this.matSnackBar.open(response.error.replace(/['"]+/g, ''), 'Cancel', {
            duration: 4000
          });
        }
      );
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
