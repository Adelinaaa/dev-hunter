import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  hidePassword: boolean = true;
  formGroup: FormGroup;
  destroy$ = new Subject<boolean>();
  isLoading = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private matSnackBar: MatSnackBar
  ) {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  private buildForm(): void {
    this.formGroup = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [
        null,
        [
          Validators.required,
          Validators.minLength(8),
          Validators.pattern(
            /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[ !"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~])[0-9a-zA-Z !"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~]+/
          )
        ]
      ]
    });
  }


  onSubmit(): void {
    if (this.formGroup.invalid) {
      return;
    }

    this.authService.login(this.formGroup.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (token) => {
          this.authService.storeToken(Object.values(JSON.parse(token))[0]);
          this.router.navigate(['']);
        },
        (response) => {
          console.log(response);
          this.matSnackBar.open(response.error.replace(/['"]+/g, ''), 'Cancel', {
            duration: 4000
          });
        }
      );
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
